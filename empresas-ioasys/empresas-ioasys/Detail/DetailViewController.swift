//
//  DetailViewController.swift
//  empresas-ioasys
//
//  Created by Pedro Gabriel on 28/02/20.
//  Copyright © 2020 Pedro Gabriel. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController, Storyboarded {
    
    var viewModel: EnterpriseDetailModel?
    let customView: DetailView = .xib()
    weak var coordinator: MainCoordinator?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customView.viewModel = self.viewModel
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = .customMediumPink
        self.navigationController?.navigationBar.isTranslucent = false
        self.view = customView
        let customBack = UIBarButtonItem.init(image: UIImage.init(named: "ic-native-back-button"), style: .plain, target: self, action: #selector(self.customBack))
        self.navigationItem.leftBarButtonItem = customBack
        self.navigationItem.leftBarButtonItem?.tintColor = .white
        self.navigationItem.title = "\(self.viewModel?.enterprise?.enterpriseName?.uppercased() ?? "Empresa\(self.viewModel?.enterprise?.id ?? -1)")"
        // Do any additional setup after loading the view.
    }
    
    @objc
    private func customBack() {
        self.coordinator?.popOverViewController()
    }

}
