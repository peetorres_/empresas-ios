//
//  DetailView.swift
//  empresas-ioasys
//
//  Created by Pedro Gabriel on 28/02/20.
//  Copyright © 2020 Pedro Gabriel. All rights reserved.
//

import UIKit

class DetailView: UIView, NibLoadable {
    
    @IBOutlet weak var imvEnterprise: UIImageView!
    @IBOutlet weak var lblDescription: UILabel!
    
    var viewModel: EnterpriseDetailModel? {
        didSet {
            guard let _viewModel = self.viewModel else { return }
            self.lblDescription.text = _viewModel.enterprise?.description
        }
    }
    
    override func awakeFromNib() {
        self.backgroundColor = .customBackground
        self.imvEnterprise.image = UIImage(named: "img_e_1_lista")
        self.lblDescription.text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
        self.lblDescription.font = .systemFont(ofSize: 17)
        self.lblDescription.textColor = .customFontGray
    }

}
