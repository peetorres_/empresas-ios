//
//  EnterpriseListTableViewCell.swift
//  empresas-ioasys
//
//  Created by Pedro Gabriel on 28/02/20.
//  Copyright © 2020 Pedro Gabriel. All rights reserved.
//

import UIKit

class EnterpriseListTableViewCell: UITableViewCell {

    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var imvEnterprise: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblContentTop: UILabel!
    @IBOutlet weak var lblContentBottom: UILabel!
    
    var viewModel: EnterprisesModel? {
        didSet {
            let title = "\(self.viewModel?.enterpriseName ?? "Empresa\(self.viewModel?.id ?? -1)")"
            let enterpriseTypeName = self.viewModel?.enterpriseType?.enterpriseTypeName ?? ""
            let country = self.viewModel?.country ?? ""
            self.setupTexts(title: title, type: enterpriseTypeName, country: country)
        }
    }
    
    func setupTexts(title: String, type: String, country: String) {
        self.lblTitle.text = title
        self.lblContentTop.text = type
        self.lblContentBottom.text = country
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.viewBackground.setCorner(radius: 3)
        self.imvEnterprise.image = UIImage.init(named: "img_e_1_lista")
        self.lblTitle.font = .systemFont(ofSize: 17, weight: .bold)
        self.lblContentTop.font = .italicSystemFont(ofSize: 17)
        self.lblContentBottom.font = .systemFont(ofSize: 17)
        self.selectionStyle = .none
    }
}

extension UIView {
    func setCorner(radius: CGFloat) {
        self.layer.masksToBounds = true
        self.layer.cornerRadius = radius
    }
}
