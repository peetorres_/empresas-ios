//
//  EmptyTableViewCell.swift
//  empresas-ioasys
//
//  Created by Pedro Gabriel on 28/02/20.
//  Copyright © 2020 Pedro Gabriel. All rights reserved.
//

import UIKit

class EmptyTableViewCell: UITableViewCell {

    @IBOutlet weak var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.label.text = "Nenhuma empresa foi encontrada para a busca realizada."
        self.label.textAlignment = .center
        self.label.font = .systemFont(ofSize: 17)
        self.label.textColor = .customGrayLight
        self.selectionStyle = .none
        self.isUserInteractionEnabled = false
    }
}
