//
//  HomeView.swift
//  empresas-ioasys
//
//  Created by Pedro Gabriel on 28/02/20.
//  Copyright © 2020 Pedro Gabriel. All rights reserved.
//

import UIKit

protocol HomeViewDelegate {
    func home(_ view: HomeView, didTouchOn indexPath: IndexPath)
}

class HomeView: UIView, NibLoadable {
    
    @IBOutlet weak var lblContent: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var viewModel: EnterpriseDataModel? {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    var isSearching = false {
        didSet {
            self.lblContent.isHidden = isSearching
            self.tableView.isHidden = !isSearching
            self.tableView.reloadData()
        }
    }
    
    var delegate: HomeViewDelegate?
    
    override func awakeFromNib() {
        self.backgroundColor = .customBackground
        self.lblContent.textColor = .customFontGray
        self.lblContent.text = "Clique na busca para iniciar."
        self.lblContent.font = .systemFont(ofSize: 16)
        self.registerCells()
        self.tableView.backgroundColor = .clear
        self.tableView.separatorStyle = .none
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 150.0
    }
    
    func registerCells() {
        self.tableView.register(UINib(nibName: "EmptyTableViewCell", bundle: Bundle(for: EmptyTableViewCell.self)), forCellReuseIdentifier: "EmptyTableViewCell")
        self.tableView.register(UINib(nibName: "EnterpriseListTableViewCell", bundle: Bundle(for: EmptyTableViewCell.self)), forCellReuseIdentifier: "EnterpriseListTableViewCell")
    }
}



extension HomeView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let count = self.viewModel?.enterprises?.count else { return 0 }
        return count == 0 ? 1 : count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let row = indexPath.row
        let count = self.viewModel?.enterprises?.count ?? 0
        if count > 0 && isSearching {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EnterpriseListTableViewCell", for: indexPath) as! EnterpriseListTableViewCell
            cell.viewModel = self.viewModel?.enterprises?[row]
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EmptyTableViewCell", for: indexPath) as! EmptyTableViewCell
            return cell
        }
    }
}

extension HomeView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.home(self, didTouchOn: indexPath)
    }
}
