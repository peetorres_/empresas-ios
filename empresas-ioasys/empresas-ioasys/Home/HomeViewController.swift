//
//  HomeViewController.swift
//  empresas-ioasys
//
//  Created by Pedro Gabriel on 28/02/20.
//  Copyright © 2020 Pedro Gabriel. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, Storyboarded {
    
    var viewModel: EnterpriseDataModel?
    var customView: HomeView = .xib()
    weak var coordinator: MainCoordinator?
    lazy var searchBar:UISearchBar = UISearchBar(frame: .init(x: 0, y: 0, width: self.view.bounds.width - 20, height: 10))
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.barTintColor = .customMediumPink
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationItem.setHidesBackButton(true, animated:true)
        self.setupNotSearchingState()
        self.view = customView
        self.customView.delegate = self
    }
    
    func setupNotSearchingState() {
        self.customView.isSearching = false
        self.navigationItem.leftBarButtonItem = nil
        let rightNavButton = UIBarButtonItem.init(image: UIImage.init(systemName: "magnifyingglass"), style: .plain, target: self, action: #selector(self.customSearch))
               self.navigationItem.rightBarButtonItem = rightNavButton
               self.navigationItem.rightBarButtonItem?.tintColor = .white
               
       let imageView = UIImageView(image: UIImage(named: "logo_nav"))
       imageView.contentMode = UIView.ContentMode.scaleAspectFit
       let titleView = UIView(frame: CGRect(x: 0, y: 0, width: 117, height: 44))
           imageView.frame = titleView.bounds
           titleView.addSubview(imageView)
       self.navigationItem.titleView = titleView
    }
    
    func setupSearchState() {
        self.customView.isSearching = true
        searchBar.searchTextField.tintColor = .white
        searchBar.searchTextField.textColor = .white
        searchBar.searchTextField.placeholder = "Pesquisar"
        searchBar.searchTextField.backgroundColor = .customMediumPink
        searchBar.tintColor = .white
        let leftNavBarButton = UIBarButtonItem(customView: searchBar)
        self.navigationItem.leftBarButtonItem = leftNavBarButton
        searchBar.searchTextField.becomeFirstResponder()
        searchBar.searchTextField.addTarget(self, action: #selector(self.searchTextFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        
        self.navigationItem.titleView = nil
        self.navigationItem.rightBarButtonItem = nil
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        tapGesture.cancelsTouchesInView = false
        self.customView.addGestureRecognizer(tapGesture)
    }
    
    func apiServiceSearch(text: String) {
        AuthenticationAPI.search(enterpriseTypes: [1], name: text) { (response, error, cache) in
            if let data = response {
                self.viewModel = data
                self.customView.viewModel = self.viewModel
            } else if let error = error {
                print(error.errorDescription as Any)
            }
        }
    }
    
    func apiServiceShowDetail(_ identifier: Int) {
        self.hideKeyboard()
        self.customView.tableView.showBlurLoader()
        AuthenticationAPI.showDetail(id: identifier) { (response, error, cache) in
            self.customView.tableView.removeBlurLoader()
            if let data = response {
                self.coordinator?.showDetail(_viewModel: data)
            } else if let error = error {
                print(error.errorDescription as Any)
            }
        }
    }
    
    @objc
    func searchTextFieldDidChange(_ textField: UITextField) {
        guard let text = textField.text else { return }
        if !text.isEmpty {
            self.apiServiceSearch(text: text)
        } else {
            self.viewModel = nil
            self.customView.viewModel = self.viewModel
        }
    }
    
    @objc
    private func customSearch() {
        self.setupSearchState()
    }
    
    @objc
    func hideKeyboard() {
        self.searchBar.searchTextField.endEditing(true)
    }
    
}

extension HomeViewController: HomeViewDelegate {
    func home(_ view: HomeView, didTouchOn indexPath: IndexPath) {
        guard let identifier = self.viewModel?.enterprises?[indexPath.row].id else { return }
        self.apiServiceShowDetail(identifier)
    }
}
