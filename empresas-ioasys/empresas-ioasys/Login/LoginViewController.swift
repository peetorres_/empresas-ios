//
//  ViewController.swift
//  empresas-ioasys
//
//  Created by Pedro Gabriel on 27/02/20.
//  Copyright © 2020 Pedro Gabriel. All rights reserved.
//

import UIKit
import TPKeyboardAvoidingSwift

class LoginViewController: UIViewController, Storyboarded {

    @IBOutlet weak var contentView: UIView!
    var viewModel: LoginViewModel = .init()
    let customView: LoginView = .xib()
    weak var coordinator: MainCoordinator?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        self.view.backgroundColor = .customBackground
        self.customView.viewModel = self.viewModel
        self.customView.delegate = self
        contentView.addSubview(customView)
        self.view.layoutIfNeeded()
    }
    
    func apiServiceLogin() {
        self.customView.hideKeyboard()
        self.view.showBlurLoader()
        //email: "testeapple@ioasys.com.br", password: "12341234"
        AuthenticationAPI.loginWith(email: self.viewModel.getEmail(), password: self.viewModel.getPassword()) { (response, error, cache) in
            self.view.removeBlurLoader()
            if let userData = response {
                self.viewModel.set(userData)
                self.coordinator?.showHome()
            } else if let error = error {
                if error.errorDescription == "unauthorized" {
                    self.viewModel.isFailureState = true
                    self.customView.setFailureState()
                }
            }
        }
    }
}

extension LoginViewController: LoginViewDelegate {
    func login(_ view: LoginView, didClickOnSignIn button: UIButton) {
        self.apiServiceLogin()
    }
}

