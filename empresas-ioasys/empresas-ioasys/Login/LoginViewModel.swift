//
//  LoginViewModel.swift
//  empresas-ioasys
//
//  Created by Pedro Gabriel on 28/02/20.
//  Copyright © 2020 Pedro Gabriel. All rights reserved.
//

import UIKit

class LoginViewModel: NSObject {
    //email: "testeapple@ioasys.com.br", password: "12341234"
    private var email: String?
    private var password: String?
    private var userData: User?
    private var isPasswordHidden: Bool = false
    var isFailureState: Bool = false
}

extension LoginViewModel {
    func set(email: String) {
        self.email = email
    }
    
    func set(password: String) {
        self.password = password
    }
    
    func getEmail() -> String {
        return self.email ?? ""
    }
    
    func getPassword() -> String {
        return self.password ?? ""
    }
    
    func set(_ userData: User) {
        self.userData = userData
    }
    
    func set(isPasswordHidden: Bool) {
        self.isPasswordHidden = isPasswordHidden
    }
    
    func getIsPasswordHidden() -> Bool {
        return self.isPasswordHidden
    }
    
    func getImageIsPasswordHidden() -> UIImage {
        return (self.isPasswordHidden ? UIImage(systemName: "eye") :  UIImage(systemName: "eye.fill"))!
    }
}
