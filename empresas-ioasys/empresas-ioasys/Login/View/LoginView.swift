//
//  LoginView.swift
//  empresas-ioasys
//
//  Created by Pedro Gabriel on 27/02/20.
//  Copyright © 2020 Pedro Gabriel. All rights reserved.
//

import UIKit

protocol LoginViewDelegate {
    func login(_ view: LoginView, didClickOnSignIn button: UIButton)
}

class LoginView: UIView, NibLoadable {
    
    @IBOutlet weak var imvLogo: UIImageView!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblContent: UILabel!
    
    @IBOutlet weak var imvEmail: UIImageView!
    @IBOutlet weak var txfLogin: UITextField!
    @IBOutlet weak var imvRightTop: UIImageView!
    @IBOutlet weak var viewLineTop: UIView!
    
    @IBOutlet weak var imvLock: UIImageView!
    @IBOutlet weak var txfPassword: PasswordTextField!
    @IBOutlet weak var imvRightBottom: UIImageView!
    @IBOutlet weak var btnPasswordVisibility: UIButton!
    @IBOutlet weak var viewLineBottom: UIView!
    
    @IBOutlet weak var lblError: UILabel!
    @IBOutlet weak var constHeightLblError: NSLayoutConstraint!
    
    @IBOutlet weak var btnSignIn: UIButton!
    
    @IBAction func btnSignInClicked(_ sender: UIButton) {
        self.delegate?.login(self, didClickOnSignIn: sender)
    }
    
    @IBAction func changePasswordVisibility(_ sender: Any) {
        
        guard let isHidden = self.viewModel?.getIsPasswordHidden() else { return }
        self.viewModel?.set(isPasswordHidden: !isHidden)
        self.txfPassword.isSecureTextEntry = self.viewModel?.getIsPasswordHidden() ?? false
        self.btnPasswordVisibility.setImage(self.viewModel?.getImageIsPasswordHidden(), for: .normal)
    }
    
    var viewModel: LoginViewModel? {
        didSet {
            self.btnPasswordVisibility.setImage(self.viewModel?.getImageIsPasswordHidden(), for: .normal)
        }
    }
    
    var delegate: LoginViewDelegate?
    
    override func awakeFromNib() {
        self.backgroundColor = .customBackground
        
        self.setupLabels()
        self.setupButtons()
        self.setupTextFields()
        self.setupViewsAndImages()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        self.addGestureRecognizer(tapGesture)
        self.setDefaultState()
    }
    
    func setupLabels() {
        self.lblTitle.text = "BEM-VINDO AO\nEMPRESAS"
        self.lblTitle.textAlignment = .center
        self.lblTitle.font = .systemFont(ofSize: 20, weight: .bold)
        self.lblTitle.textColor = .black
        
        self.lblContent.text = "Lorem ipsum dolor sit amet, contetur adipiscing elit. Nunc accumsan."
        self.lblContent.textAlignment = .center
        self.lblContent.font = .systemFont(ofSize: 16)
        self.lblContent.textColor = .black
        
        self.lblError.text = "Credenciais informadas são inválidas, tente novamente."
        self.lblError.font = .systemFont(ofSize: 8)
        self.lblError.textColor = .customMediumPink
        self.constHeightLblError.priority = UILayoutPriority(rawValue: 1)
    }
    
    func setupTextFields() {
        self.txfLogin.placeholder = "E-mail"
        self.txfLogin.font = .systemFont(ofSize: 16)
        self.txfLogin.textColor = .customFontGray
        self.txfLogin.attributedPlaceholder = NSAttributedString(string: "E-mail",
                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor.customGrayLight])
        self.txfLogin.keyboardType = .emailAddress
        self.txfLogin.clearsOnBeginEditing = false
        self.txfLogin.addTarget(self, action: #selector(self.textFieldTopDidChange(_:)), for: UIControl.Event.editingChanged)
        
        self.txfPassword.textColor = .customFontGray
        self.txfPassword.attributedPlaceholder = NSAttributedString(string: "Senha",
                                                                    attributes: [NSAttributedString.Key.foregroundColor: UIColor.customGrayLight])
        self.txfPassword.font = .systemFont(ofSize: 16)
        self.txfPassword.clearsOnBeginEditing = false
        self.txfPassword.addTarget(self, action: #selector(self.textFieldBottomDidChange(_:)), for: UIControl.Event.editingChanged)
    }
    
    func setupButtons() {
        self.btnPasswordVisibility.tintColor = .customFontGray
        self.btnSignIn.backgroundColor = .customGreenyBlue
        self.btnSignIn.setTitle("ENTRAR", for: .normal)
        self.btnSignIn.titleLabel?.font = .systemFont(ofSize: 15, weight: .bold)
        self.btnSignIn.setTitleColor(.customWhite, for: .normal)
        self.btnSignIn.layer.cornerRadius = 3
    }
    
    func setupViewsAndImages() {
        self.imvLogo.image = UIImage(named: "logo_home")
        self.imvEmail.image = UIImage(named: "ic_email")
        self.imvRightTop.image = UIImage(systemName: "exclamationmark.circle.fill")
        self.imvRightTop.tintColor = .customMediumPink
        self.imvRightTop.isHidden = true
        self.viewLineTop.backgroundColor = .customMediumPink
        self.imvLock.image = UIImage(named: "ic_cadeado")
        self.imvRightBottom.image = UIImage(systemName: "exclamationmark.circle.fill")
        self.imvRightBottom.tintColor = .customMediumPink
        self.imvRightBottom.isHidden = true
        self.viewLineBottom.backgroundColor = .customMediumPink
    }
    
    func setDefaultState() {
        self.imvRightTop.isHidden = true
        self.viewLineTop.backgroundColor = .customFontGray
        self.imvRightBottom.isHidden = true
        self.btnPasswordVisibility.isHidden = (self.viewModel?.getPassword().count ?? 0) > 0 ? false : true
        self.btnPasswordVisibility.setImage(self.viewModel?.getImageIsPasswordHidden(), for: .normal)
        self.viewLineBottom.backgroundColor = .customFontGray
        self.constHeightLblError.priority = UILayoutPriority(rawValue: 1000)
        self.btnSignIn.isEnabled = true
        self.btnSignIn.backgroundColor = .customGreenyBlue
        self.btnSignIn.alpha = CGFloat(1.0)
    }
    
    func setFailureState() {
        self.imvRightTop.isHidden = false
        self.viewLineTop.backgroundColor = .customMediumPink
        self.imvRightBottom.isHidden = false
        self.btnPasswordVisibility.isHidden = true
        self.viewLineBottom.backgroundColor = .customMediumPink
        self.constHeightLblError.priority = UILayoutPriority(rawValue: 1)
        self.btnSignIn.isEnabled = false
        self.btnSignIn.backgroundColor = .customCharcoalGray
        self.btnSignIn.alpha = CGFloat(0.5)
    }
    
    @objc
    func textFieldTopDidChange(_ textField: UITextField) {
        guard let text = textField.text else { return }
        self.viewModel?.set(email: text)
        if self.viewModel?.isFailureState ?? true {
            self.setDefaultState()
        }
    }
    
    @objc
    func textFieldBottomDidChange(_ textField: UITextField) {
        guard let text = textField.text else { return }
        self.viewModel?.set(password: text)
        self.btnPasswordVisibility.isHidden = (self.viewModel?.getPassword().count ?? 0) > 0 ? false : true
        if self.viewModel?.isFailureState ?? true {
            self.setDefaultState()
        }
    }
    
    @objc
    func hideKeyboard() {
        self.endEditing(true)
    }
}

class PasswordTextField: UITextField {

    override var isSecureTextEntry: Bool {
        didSet {
            if isFirstResponder {
                _ = becomeFirstResponder()
                //MARK:- Do something what you want
            }
        }
    }

    override func becomeFirstResponder() -> Bool {
        let success = super.becomeFirstResponder()
        if isSecureTextEntry, let text = self.text {
            self.text?.removeAll()
            insertText(text)
        }
         return success
    }
}
