//
//  ColorPalette.swift
//  empresas-ioasys
//
//  Created by Pedro Gabriel on 27/02/20.
//  Copyright © 2020 Pedro Gabriel. All rights reserved.
//

import UIKit

public extension UIColor {
    /// Hex: #FFFFFF
    static var customWhite: UIColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    
    /// Hex: #ACABAB
    static var customGrayLight: UIColor = #colorLiteral(red: 0.6745098039, green: 0.6705882353, blue: 0.6705882353, alpha: 1)
    
    /// Hex: #EBE9D7
    static var customBackground: UIColor = #colorLiteral(red: 0.9215686275, green: 0.9137254902, blue: 0.8431372549, alpha: 1)

    /// Hex: #EE4C77
    static var customMediumPink: UIColor = #colorLiteral(red: 0.9333333333, green: 0.2980392157, blue: 0.4666666667, alpha: 1)
    
    /// Hex: #57BBBC
    static var customGreenyBlue: UIColor = #colorLiteral(red: 0.3411764706, green: 0.7333333333, blue: 0.737254902, alpha: 1)
    
    /// Hex: #748383
    static var customGrayMarine: UIColor = #colorLiteral(red: 0.4549019608, green: 0.5137254902, blue: 0.5137254902, alpha: 1)
    
    /// Hex: #383743
    static var customCharcoalGray: UIColor = #colorLiteral(red: 0.2196078431, green: 0.2156862745, blue: 0.262745098, alpha: 1)
    
    /// Hex: #494758
    static var customFontGray: UIColor = #colorLiteral(red: 0.2862745098, green: 0.2784313725, blue: 0.3450980392, alpha: 1)
    
}
