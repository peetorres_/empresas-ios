//
//  NibLoadable.swift
//  empresas-ioasys
//
//  Created by Pedro Gabriel on 27/02/20.
//  Copyright © 2020 Pedro Gabriel. All rights reserved.
//

import UIKit

public protocol NibLoadable {
    static var nibName: String { get }
}

public extension NibLoadable where Self: UIView {
    
    static var nibName: String {
        return String(describing: Self.self)
    }
    
    static var nib: UINib {
        let bundle = Bundle(for: Self.self)
        return UINib(nibName: Self.nibName, bundle: bundle)
    }
    
    static func xib() -> Self {
        return UINib.instanceFromXib(Self.self)
    }

}


public extension UINib {
    
    static func instanceFromXib<ViewClass: UIView>(_ viewClassType: ViewClass.Type) -> ViewClass {
        let bundle = Bundle(for: ViewClass.self)
        let views = UINib(nibName: String(describing: ViewClass.self), bundle: bundle)
            .instantiate(withOwner: nil, options: nil) as! [UIView]
        return views.first as! ViewClass
    }
    
}


public extension NibLoadable where Self: UIView {
    
    func setupFromNib() {
        guard let view = Self.nib.instantiate(withOwner: self, options: nil).first as? UIView else { fatalError("Error loading \(self) from nib") }
        self.addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
    }
    
}

open class UIXibView: UIView, NibLoadable {
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        self.setupUI()
    }
    
    open func setupUI() {
        
    }
    
}
