//
//  UserModel.swift
//  empresas-ioasys
//
//  Created by Pedro Gabriel on 28/02/20.
//  Copyright © 2020 Pedro Gabriel. All rights reserved.
//

import UIKit

struct User: Mappable {
    var enterprise: String?
    var investor: InvestorModel?
    var success: Int?
    
    init(mapper: Mapper) {
        self.enterprise = mapper.keyPath("enterprise")
        self.investor = mapper.keyPath("investor")
        self.success = mapper.keyPath("success")
    }
}

struct InvestorModel: Mappable {
    init(mapper: Mapper) {
        self.balance = mapper.keyPath("balance")
        self.city = mapper.keyPath("city")
        self.country = mapper.keyPath("country")
        self.email = mapper.keyPath("email")
        self.firstAccess = mapper.keyPath("first_access")
        self.id = mapper.keyPath("id")
        self.investorName = mapper.keyPath("investor_name")
        self.photo = mapper.keyPath("photo")
        self.portfolio = mapper.keyPath("portfolio")
        self.portfolioValue = mapper.keyPath("portfolio_value")
        self.superAngel = mapper.keyPath("super_angel")
    }
    
    var balance: Int?
    var city: String?
    var country: String?
    var email: String?
    var firstAccess: Int?
    var id: Int?
    var investorName: String?
    var photo: String?
    var portfolio: PortfolioModel?
    var portfolioValue: Int?
    var superAngel: Int?
}

struct PortfolioModel: Mappable {
    
    var enterprises: [String]?
    var enterprisesNumber: Int?
    
    init(mapper: Mapper) {
        self.enterprises = mapper.keyPath("enterprises")
        self.enterprisesNumber = mapper.keyPath("enterprises_number")
    }
}
