//
//  EnterprisesModel.swift
//  empresas-ioasys
//
//  Created by Pedro Gabriel on 28/02/20.
//  Copyright © 2020 Pedro Gabriel. All rights reserved.
//

import UIKit

struct EnterpriseDetailModel: Mappable {
    var enterprise: EnterprisesModel?
    var success: Int?
    init(mapper: Mapper) {
        self.success = mapper.keyPath("success")
        self.enterprise = mapper.keyPath("enterprise")
    }
}

struct EnterpriseDataModel: Mappable {
    var enterprises: [EnterprisesModel]?
    init(mapper: Mapper) {
        self.enterprises = mapper.keyPath("enterprises")
    }
}

struct EnterprisesModel: Mappable {
    var city: String?
    var country: String?
    var description: String?
    var emailEnterprise: String?
    var enterpriseName: String?
    var enterpriseType: EnterpriseModel?
    var facebook: String?
    var id: Int?
    var linkedin: String?
    var ownEnterprise: Int?
    var phone: String?
    var photo: String?
    var sharePrice: Int?
    var twitter: String?
    var value: Int?
    
    init(mapper: Mapper) {
        self.city = mapper.keyPath("city")
        self.country = mapper.keyPath("country")
        self.description = mapper.keyPath("description")
        self.emailEnterprise = mapper.keyPath("email_enterprise")
        self.enterpriseName = mapper.keyPath("enterprise_name")
        self.enterpriseType = mapper.keyPath("enterprise_type")
        self.facebook = mapper.keyPath("facebook")
        self.id = mapper.keyPath("id")
        self.linkedin = mapper.keyPath("linkedin")
        self.ownEnterprise = mapper.keyPath("own_enterprise")
        self.phone = mapper.keyPath("phone")
        self.photo = mapper.keyPath("photo")
        self.sharePrice = mapper.keyPath("sharePrice")
        self.twitter = mapper.keyPath("twitter")
        self.value = mapper.keyPath("value")
    }
}

struct EnterpriseModel: Mappable {
    init(mapper: Mapper) {
        self.enterpriseTypeName = mapper.keyPath("enterprise_type_name")
        self.id = mapper.keyPath("id")
    }
    var enterpriseTypeName: String?
    var id: Int?
}
