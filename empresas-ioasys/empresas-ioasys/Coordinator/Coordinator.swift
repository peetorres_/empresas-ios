//
//  Coordinator.swift
//  empresas-ioasys
//
//  Created by Pedro Gabriel on 28/02/20.
//  Copyright © 2020 Pedro Gabriel. All rights reserved.
//

import UIKit

protocol Coordinator {
    var childCoordinators: [Coordinator] { get set }
    var navigationController: UINavigationController { get set }

    func start()
}

