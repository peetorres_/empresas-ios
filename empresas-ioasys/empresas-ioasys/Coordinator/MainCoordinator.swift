//
//  MainCoordinator.swift
//  empresas-ioasys
//
//  Created by Pedro Gabriel on 28/02/20.
//  Copyright © 2020 Pedro Gabriel. All rights reserved.
//

import UIKit

class MainCoordinator: Coordinator {
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    func start() {
        let vc = LoginViewController.instantiate()
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: false)
    }
    
    func showHome() {
        let vc = HomeViewController.instantiate()
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: true)
    }
    
    func showDetail(_viewModel: EnterpriseDetailModel) {
        let vc = DetailViewController.instantiate()
        vc.viewModel = _viewModel
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: true)
    }
    
    func popOverViewController() {
        self.navigationController.popViewController(animated: true)
    }
}
