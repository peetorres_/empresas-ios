//
//  UserAPI.swift
//  iOS Helpers Swift
//
//  Created by Jota Melo on 15/02/17.
//  Copyright © 2017 Jota. All rights reserved.
//

import UIKit



class AuthenticationAPI: APIRequest {
    
    override init(method: API.HTTPMethod, path: String, parameters: [String : Any]?, urlParameters: [String : Any]?, cacheOption: API.CacheOption, completion: ResponseBlock<Any>?) {
        super.init(method: method, path: path, parameters: parameters, urlParameters: urlParameters, cacheOption: cacheOption, completion: completion)
    }
    
    @discardableResult
    static func loginWith(email: String, password: String, callback: ResponseBlock<User>?) -> AuthenticationAPI {
        let request = AuthenticationAPI(method: .post, path: "users/auth/sign_in", parameters: ["email": email, "password": password], urlParameters: nil, cacheOption: .networkOnly) { (response, error, cache) in
            if let error = error {
                callback?(nil, error, cache)
            } else if let response = response as? [String: Any] {
                let user = User(dictionary: response)
                callback?(user, nil, cache)
            }
        }
        request.shouldSaveInCache = false
        request.makeRequest()
        return request
    }
    
    @discardableResult
    static func search(enterpriseTypes: [Int], name: String, callback: ResponseBlock<EnterpriseDataModel>?) -> AuthenticationAPI {
        var path = "enterprises?"
        if !enterpriseTypes.isEmpty {
            path.append("enterprise_types=")
            enterpriseTypes.forEach { (type) in
                path.append("\(type)")
            }
        }
        
        let _name = name.replace(occurences: [" "], with: "")
        if !_name.isEmpty {
            path.append(path.last != "?" ? "&" : "")
            path.append("name=" + _name)
        }
        
        let request = AuthenticationAPI(method: .get, path: path, parameters: nil, urlParameters: nil, cacheOption: .networkOnly) { (response, error, cache) in
            if let error = error {
                callback?(nil, error, cache)
            } else if let response = response as? [String: Any] {
                let enterprises = EnterpriseDataModel(dictionary: response)
                callback?(enterprises, nil, cache)
            }
        }
        request.shouldSaveInCache = false
        request.makeRequest()
        return request
    }
    
    @discardableResult
    static func showDetail(id: Int, callback: ResponseBlock<EnterpriseDetailModel>?) -> AuthenticationAPI {
        let path = "enterprises/\(id)"
        let request = AuthenticationAPI(method: .get, path: path, parameters: nil, urlParameters: nil, cacheOption: .networkOnly) { (response, error, cache) in
            if let error = error {
                callback?(nil, error, cache)
            } else if let response = response as? [String: Any] {
                let enterprises = EnterpriseDetailModel(dictionary: response)
                callback?(enterprises, nil, cache)
            }
        }
        request.shouldSaveInCache = false
        request.makeRequest()
        return request
    }
}

extension String {
    func replace(occurences: [String], with string: String) -> String {
        var newString = self
        for occur in occurences {
            newString = newString.replacingOccurrences(of: occur, with: string)
        }
        return newString
    }
}
